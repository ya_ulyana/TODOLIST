const form = document.getElementById('todoform');
const todoInput = document.getElementById('newtodo');
const todosListEl = document.getElementById("todosList");
const notificationEl = document.querySelector('.notification');

let todos = JSON.parse(localStorage.getItem('todos')) || [];
let EditTodoId = -1;
let editTodoIndex = null;

renderTodos();

form.addEventListener('submit', function (event){
    event.preventDefault();
    saveTodo();
    renderTodos();
    localStorage.setItem('todos',JSON.stringify(todos));
});

function saveTodo(){
    const todoValue = todoInput.value;
    const isEmpty = todoValue.trim() === ''; 
    const isDuplicate = todos.some((todo) => todo.value.toUpperCase()=== todoValue.toUpperCase());
 
    if(isEmpty){
        showNotification('Вы ничего не спланировали :(');
    } else if(isDuplicate){
        showNotification('Ой,кажется такое дело уже есть');
      
    } else {
        if(EditTodoId >=0){
            todos = todos.map((todo,index) => ({
                ...todo,
                value: index === EditTodoId ? todoValue : todo.value,
            }));
            EditTodoId =-1;
        }else{
            todos.push({  
                value: todoValue,
                checked: false,
                color: '#' + Math.floor(Math.random()*16777215).toString(16)
            });
        }

        todoInput.value ='';
    }
};

function renderTodos() {
    if(todos.length === 0){
        todosListEl.innerHTML ='<center>нет запланированных дел</center>';
        return;
    }

    todosListEl.innerHTML = '';

    todos.forEach((todo, index) => {
       
        const todoEl = document.createElement("div");
        todoEl.className = "todo";
        todoEl.id = index;
        
        if (index === editTodoIndex){
            const input = document.createElement('input');
            input.value = todo.value;
            input.className = "input"; 
            const editIconNew = document.createElement("button");
            editIconNew.className = "save_button"; 
            editIconNew.textContent = 'save';

            editIconNew.onclick = () => {
            editTodoIndex = null;
            if (todo.value === todo.input) {

                renderTodos();
                localStorage.setItem('todos',JSON.stringify(todos));  
            } else{
                todos[index].value = input.value;
                renderTodos();
                localStorage.setItem('todos',JSON.stringify(todos));  
            } 
            }
            todoEl.appendChild(input);
            todoEl.appendChild(editIconNew);
            todosListEl.appendChild(todoEl);
            } else {
            const checkIcon = document.createElement("icon");
            checkIcon.className = `bi ${todo.checked ? 'bi-check-circle-fill' : 'bi-circle' }`;
            checkIcon.style.color = todo.color;
            checkIcon.setAttribute("data-action", "check");
            todoEl.appendChild(checkIcon);
          
            const todoText = document.createElement("p");
            todoText.className = `bi ${todo.checked ? 'checked' : '' }`;
            todoText.setAttribute("data-action", "check");
            todoText.textContent = todo.value;
            todoEl.appendChild(todoText);
          
            const editIcon = document.createElement("i");
            editIcon.onclick = () => {
                editTodoIndex = index;
                renderTodos();
            }
            editIcon.className = "bi bi-pencil-square";
            todoEl.appendChild(editIcon);
          
            const deleteIcon = document.createElement("i");
            deleteIcon.setAttribute("data-action", "delete");
            deleteIcon.className = "bi bi-trash";
            todoEl.appendChild(deleteIcon);
            todosListEl.appendChild(todoEl);
        }
      });   
};

todosListEl.addEventListener('click', (event) => {
    const target = event.target; 
    const parentElement = target.parentNode;
    if(parentElement.className !=='todo') return;

    const todo = parentElement;
    const todoId = Number(todo.id);

    const action = target.dataset.action;
    action === 'check' && checkTodo(todoId);
    action === 'delete' && deleteTodo(todoId);
});

function checkTodo(todoId){
    todos = todos.map((todo, index) => (
        {
            value: todo.value,
            color: todo.color,
            checked: index === todoId ? !todo.checked : todo.checked       
        }));

    renderTodos();  
    localStorage.setItem('todos',JSON.stringify(todos));
}

function deleteTodo(todoId){
    todos = todos.filter((todo, index) => index !==todoId); 
    EditTodoId =-1;
    renderTodos();
    localStorage.setItem('todos',JSON.stringify(todos));
}

function showNotification(msg) {
    notificationEl.innerHTML = msg;
    notificationEl.classList.add('notif-enter'); 
    setTimeout(() => {      
        notificationEl.classList.remove('notif-enter');
    }, 2000);
}

document.querySelector('.delete_all_todos').addEventListener('click',()=>{
    todos = [];
    renderTodos();
    localStorage.setItem('todos',JSON.stringify(todos));    
});

function delete_done_todos(){
    todos = todos.filter(el => el.checked === false);
    renderTodos();
    localStorage.setItem('todos',JSON.stringify(todos));   
}
document.querySelector('.delete_done_todos').addEventListener('click', (delete_done_todos));
    


